// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.29.0
// 	protoc        v3.19.4
// source: config_api.proto

package config_api

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ServerConfigInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	CanUserRegister   bool `protobuf:"varint,1,opt,name=canUserRegister,proto3" json:"canUserRegister,omitempty"`     //是否可以注册用户
	QuickUserRegister bool `protobuf:"varint,2,opt,name=quickUserRegister,proto3" json:"quickUserRegister,omitempty"` //是否支持快捷注册
}

func (x *ServerConfigInfo) Reset() {
	*x = ServerConfigInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_config_api_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ServerConfigInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ServerConfigInfo) ProtoMessage() {}

func (x *ServerConfigInfo) ProtoReflect() protoreflect.Message {
	mi := &file_config_api_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ServerConfigInfo.ProtoReflect.Descriptor instead.
func (*ServerConfigInfo) Descriptor() ([]byte, []int) {
	return file_config_api_proto_rawDescGZIP(), []int{0}
}

func (x *ServerConfigInfo) GetCanUserRegister() bool {
	if x != nil {
		return x.CanUserRegister
	}
	return false
}

func (x *ServerConfigInfo) GetQuickUserRegister() bool {
	if x != nil {
		return x.QuickUserRegister
	}
	return false
}

type GetServerConfigRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *GetServerConfigRequest) Reset() {
	*x = GetServerConfigRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_config_api_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetServerConfigRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetServerConfigRequest) ProtoMessage() {}

func (x *GetServerConfigRequest) ProtoReflect() protoreflect.Message {
	mi := &file_config_api_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetServerConfigRequest.ProtoReflect.Descriptor instead.
func (*GetServerConfigRequest) Descriptor() ([]byte, []int) {
	return file_config_api_proto_rawDescGZIP(), []int{1}
}

type GetServerConfigResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Config *ServerConfigInfo `protobuf:"bytes,1,opt,name=config,proto3" json:"config,omitempty"`
}

func (x *GetServerConfigResponse) Reset() {
	*x = GetServerConfigResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_config_api_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetServerConfigResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetServerConfigResponse) ProtoMessage() {}

func (x *GetServerConfigResponse) ProtoReflect() protoreflect.Message {
	mi := &file_config_api_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetServerConfigResponse.ProtoReflect.Descriptor instead.
func (*GetServerConfigResponse) Descriptor() ([]byte, []int) {
	return file_config_api_proto_rawDescGZIP(), []int{2}
}

func (x *GetServerConfigResponse) GetConfig() *ServerConfigInfo {
	if x != nil {
		return x.Config
	}
	return nil
}

var File_config_api_proto protoreflect.FileDescriptor

var file_config_api_proto_rawDesc = []byte{
	0x0a, 0x10, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x5f, 0x61, 0x70, 0x69, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x0a, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x5f, 0x61, 0x70, 0x69, 0x22, 0x6a,
	0x0a, 0x10, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x49, 0x6e,
	0x66, 0x6f, 0x12, 0x28, 0x0a, 0x0f, 0x63, 0x61, 0x6e, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x67,
	0x69, 0x73, 0x74, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0f, 0x63, 0x61, 0x6e,
	0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x65, 0x72, 0x12, 0x2c, 0x0a, 0x11,
	0x71, 0x75, 0x69, 0x63, 0x6b, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x65,
	0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x11, 0x71, 0x75, 0x69, 0x63, 0x6b, 0x55, 0x73,
	0x65, 0x72, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x65, 0x72, 0x22, 0x18, 0x0a, 0x16, 0x47, 0x65,
	0x74, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x22, 0x4f, 0x0a, 0x17, 0x47, 0x65, 0x74, 0x53, 0x65, 0x72, 0x76, 0x65,
	0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x34, 0x0a, 0x06, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1c, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x5f, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x65, 0x72,
	0x76, 0x65, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x06, 0x63,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x32, 0x69, 0x0a, 0x09, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x41,
	0x70, 0x69, 0x12, 0x5c, 0x0a, 0x0f, 0x67, 0x65, 0x74, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43,
	0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x22, 0x2e, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x5f, 0x61,
	0x70, 0x69, 0x2e, 0x47, 0x65, 0x74, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x6f, 0x6e, 0x66,
	0x69, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x23, 0x2e, 0x63, 0x6f, 0x6e, 0x66,
	0x69, 0x67, 0x5f, 0x61, 0x70, 0x69, 0x2e, 0x47, 0x65, 0x74, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00,
	0x42, 0x0e, 0x5a, 0x0c, 0x2e, 0x2f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x5f, 0x61, 0x70, 0x69,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_config_api_proto_rawDescOnce sync.Once
	file_config_api_proto_rawDescData = file_config_api_proto_rawDesc
)

func file_config_api_proto_rawDescGZIP() []byte {
	file_config_api_proto_rawDescOnce.Do(func() {
		file_config_api_proto_rawDescData = protoimpl.X.CompressGZIP(file_config_api_proto_rawDescData)
	})
	return file_config_api_proto_rawDescData
}

var file_config_api_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_config_api_proto_goTypes = []interface{}{
	(*ServerConfigInfo)(nil),        // 0: config_api.ServerConfigInfo
	(*GetServerConfigRequest)(nil),  // 1: config_api.GetServerConfigRequest
	(*GetServerConfigResponse)(nil), // 2: config_api.GetServerConfigResponse
}
var file_config_api_proto_depIdxs = []int32{
	0, // 0: config_api.GetServerConfigResponse.config:type_name -> config_api.ServerConfigInfo
	1, // 1: config_api.ConfigApi.getServerConfig:input_type -> config_api.GetServerConfigRequest
	2, // 2: config_api.ConfigApi.getServerConfig:output_type -> config_api.GetServerConfigResponse
	2, // [2:3] is the sub-list for method output_type
	1, // [1:2] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_config_api_proto_init() }
func file_config_api_proto_init() {
	if File_config_api_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_config_api_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ServerConfigInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_config_api_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetServerConfigRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_config_api_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetServerConfigResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_config_api_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_config_api_proto_goTypes,
		DependencyIndexes: file_config_api_proto_depIdxs,
		MessageInfos:      file_config_api_proto_msgTypes,
	}.Build()
	File_config_api_proto = out.File
	file_config_api_proto_rawDesc = nil
	file_config_api_proto_goTypes = nil
	file_config_api_proto_depIdxs = nil
}
