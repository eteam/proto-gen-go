#!/bin/sh

# SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
# SPDX-License-Identifier: GPL-3.0-only

rm -rf admin_auth_api
protoc -I proto/src --go_out=. --go-grpc_out=. admin_auth_api.proto

rm -rf chat_tpl_api
protoc -I proto/src --go_out=. --go-grpc_out=. chat_tpl_api.proto

rm -rf config_api
protoc -I proto/src --go_out=. --go-grpc_out=. config_api.proto

rm -rf fs_api
protoc -I proto/src --go_out=. --go-grpc_out=. fs_api.proto

rm -rf user_api
protoc -I proto/src -I proto/src/third_part --go_out=. --go-grpc_out=. user_api.proto

rm -rf user_event_api
protoc -I proto/src -I proto/src/third_part --go_out=. --go-grpc_out=. user_event_api.proto

rm -rf team_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_api.proto

rm -rf  team_event_api
protoc -I proto/src -I proto/src/third_part --go_out=. --go-grpc_out=. team_event_api.proto

rm -rf team_disk_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_disk_api.proto

rm -rf team_issue_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_issue_api.proto

rm -rf team_issue_attr_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_issue_attr_api.proto

rm -rf team_issue_history_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_issue_history_api.proto

rm -rf team_issue_stage_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_issue_stage_api.proto

rm -rf team_issue_type_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_issue_type_api.proto

rm -rf team_knowledge_cate_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_knowledge_cate_api.proto

rm -rf team_knowledge_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_knowledge_api.proto

rm -rf team_knowledge_history_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_knowledge_history_api.proto

rm -rf team_member_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_member_api.proto

rm -rf team_workplan_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_workplan_api.proto

rm -rf team_workplan_link_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_workplan_link_api.proto

rm -rf team_workplan_history_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_workplan_history_api.proto

rm -rf team_requirement_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_requirement_api.proto

rm -rf team_requirement_history_api
protoc -I proto/src --go_out=. --go-grpc_out=. team_requirement_history_api.proto

rm -rf notices
protoc -I proto/src/notices --go_out=. --go-grpc_out=. notices_team.proto
protoc -I proto/src/notices --go_out=. --go-grpc_out=. notices_knowledge.proto
protoc -I proto/src/notices --go_out=. --go-grpc_out=. notices_issue.proto
protoc -I proto/src/notices --go_out=. --go-grpc_out=. notices_requirement.proto
protoc -I proto/src/notices --go_out=. --go-grpc_out=. notices_workplan.proto


# 去除生成代码的linsece头
find . -type f|grep -v ".git"|egrep -e "\.go$"|awk '{print "sed -i '1,3d' "$1}'|bash
