// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.19.4
// source: team_issue_attr_api.proto

package team_issue_attr_api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	TeamIssueAttrApi_AddAttr_FullMethodName    = "/team_issue_attr_api.TeamIssueAttrApi/addAttr"
	TeamIssueAttrApi_UpdateAttr_FullMethodName = "/team_issue_attr_api.TeamIssueAttrApi/updateAttr"
	TeamIssueAttrApi_ListAttr_FullMethodName   = "/team_issue_attr_api.TeamIssueAttrApi/listAttr"
	TeamIssueAttrApi_GetAttr_FullMethodName    = "/team_issue_attr_api.TeamIssueAttrApi/getAttr"
	TeamIssueAttrApi_RemoveAttr_FullMethodName = "/team_issue_attr_api.TeamIssueAttrApi/removeAttr"
)

// TeamIssueAttrApiClient is the client API for TeamIssueAttrApi service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type TeamIssueAttrApiClient interface {
	// 增加属性
	AddAttr(ctx context.Context, in *AddAttrRequest, opts ...grpc.CallOption) (*AddAttrResponse, error)
	// 更新属性
	UpdateAttr(ctx context.Context, in *UpdateAttrRequest, opts ...grpc.CallOption) (*UpdateAttrResponse, error)
	// 列出属性
	ListAttr(ctx context.Context, in *ListAttrRequest, opts ...grpc.CallOption) (*ListAttrResponse, error)
	// 获取单个属性
	GetAttr(ctx context.Context, in *GetAttrRequest, opts ...grpc.CallOption) (*GetAttrResponse, error)
	// 删除属性
	RemoveAttr(ctx context.Context, in *RemoveAttrRequest, opts ...grpc.CallOption) (*RemoveAttrResponse, error)
}

type teamIssueAttrApiClient struct {
	cc grpc.ClientConnInterface
}

func NewTeamIssueAttrApiClient(cc grpc.ClientConnInterface) TeamIssueAttrApiClient {
	return &teamIssueAttrApiClient{cc}
}

func (c *teamIssueAttrApiClient) AddAttr(ctx context.Context, in *AddAttrRequest, opts ...grpc.CallOption) (*AddAttrResponse, error) {
	out := new(AddAttrResponse)
	err := c.cc.Invoke(ctx, TeamIssueAttrApi_AddAttr_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *teamIssueAttrApiClient) UpdateAttr(ctx context.Context, in *UpdateAttrRequest, opts ...grpc.CallOption) (*UpdateAttrResponse, error) {
	out := new(UpdateAttrResponse)
	err := c.cc.Invoke(ctx, TeamIssueAttrApi_UpdateAttr_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *teamIssueAttrApiClient) ListAttr(ctx context.Context, in *ListAttrRequest, opts ...grpc.CallOption) (*ListAttrResponse, error) {
	out := new(ListAttrResponse)
	err := c.cc.Invoke(ctx, TeamIssueAttrApi_ListAttr_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *teamIssueAttrApiClient) GetAttr(ctx context.Context, in *GetAttrRequest, opts ...grpc.CallOption) (*GetAttrResponse, error) {
	out := new(GetAttrResponse)
	err := c.cc.Invoke(ctx, TeamIssueAttrApi_GetAttr_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *teamIssueAttrApiClient) RemoveAttr(ctx context.Context, in *RemoveAttrRequest, opts ...grpc.CallOption) (*RemoveAttrResponse, error) {
	out := new(RemoveAttrResponse)
	err := c.cc.Invoke(ctx, TeamIssueAttrApi_RemoveAttr_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// TeamIssueAttrApiServer is the server API for TeamIssueAttrApi service.
// All implementations must embed UnimplementedTeamIssueAttrApiServer
// for forward compatibility
type TeamIssueAttrApiServer interface {
	// 增加属性
	AddAttr(context.Context, *AddAttrRequest) (*AddAttrResponse, error)
	// 更新属性
	UpdateAttr(context.Context, *UpdateAttrRequest) (*UpdateAttrResponse, error)
	// 列出属性
	ListAttr(context.Context, *ListAttrRequest) (*ListAttrResponse, error)
	// 获取单个属性
	GetAttr(context.Context, *GetAttrRequest) (*GetAttrResponse, error)
	// 删除属性
	RemoveAttr(context.Context, *RemoveAttrRequest) (*RemoveAttrResponse, error)
	mustEmbedUnimplementedTeamIssueAttrApiServer()
}

// UnimplementedTeamIssueAttrApiServer must be embedded to have forward compatible implementations.
type UnimplementedTeamIssueAttrApiServer struct {
}

func (UnimplementedTeamIssueAttrApiServer) AddAttr(context.Context, *AddAttrRequest) (*AddAttrResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddAttr not implemented")
}
func (UnimplementedTeamIssueAttrApiServer) UpdateAttr(context.Context, *UpdateAttrRequest) (*UpdateAttrResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateAttr not implemented")
}
func (UnimplementedTeamIssueAttrApiServer) ListAttr(context.Context, *ListAttrRequest) (*ListAttrResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListAttr not implemented")
}
func (UnimplementedTeamIssueAttrApiServer) GetAttr(context.Context, *GetAttrRequest) (*GetAttrResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAttr not implemented")
}
func (UnimplementedTeamIssueAttrApiServer) RemoveAttr(context.Context, *RemoveAttrRequest) (*RemoveAttrResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveAttr not implemented")
}
func (UnimplementedTeamIssueAttrApiServer) mustEmbedUnimplementedTeamIssueAttrApiServer() {}

// UnsafeTeamIssueAttrApiServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to TeamIssueAttrApiServer will
// result in compilation errors.
type UnsafeTeamIssueAttrApiServer interface {
	mustEmbedUnimplementedTeamIssueAttrApiServer()
}

func RegisterTeamIssueAttrApiServer(s grpc.ServiceRegistrar, srv TeamIssueAttrApiServer) {
	s.RegisterService(&TeamIssueAttrApi_ServiceDesc, srv)
}

func _TeamIssueAttrApi_AddAttr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddAttrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TeamIssueAttrApiServer).AddAttr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: TeamIssueAttrApi_AddAttr_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TeamIssueAttrApiServer).AddAttr(ctx, req.(*AddAttrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TeamIssueAttrApi_UpdateAttr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateAttrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TeamIssueAttrApiServer).UpdateAttr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: TeamIssueAttrApi_UpdateAttr_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TeamIssueAttrApiServer).UpdateAttr(ctx, req.(*UpdateAttrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TeamIssueAttrApi_ListAttr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListAttrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TeamIssueAttrApiServer).ListAttr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: TeamIssueAttrApi_ListAttr_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TeamIssueAttrApiServer).ListAttr(ctx, req.(*ListAttrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TeamIssueAttrApi_GetAttr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAttrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TeamIssueAttrApiServer).GetAttr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: TeamIssueAttrApi_GetAttr_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TeamIssueAttrApiServer).GetAttr(ctx, req.(*GetAttrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TeamIssueAttrApi_RemoveAttr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RemoveAttrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TeamIssueAttrApiServer).RemoveAttr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: TeamIssueAttrApi_RemoveAttr_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TeamIssueAttrApiServer).RemoveAttr(ctx, req.(*RemoveAttrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// TeamIssueAttrApi_ServiceDesc is the grpc.ServiceDesc for TeamIssueAttrApi service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var TeamIssueAttrApi_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "team_issue_attr_api.TeamIssueAttrApi",
	HandlerType: (*TeamIssueAttrApiServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "addAttr",
			Handler:    _TeamIssueAttrApi_AddAttr_Handler,
		},
		{
			MethodName: "updateAttr",
			Handler:    _TeamIssueAttrApi_UpdateAttr_Handler,
		},
		{
			MethodName: "listAttr",
			Handler:    _TeamIssueAttrApi_ListAttr_Handler,
		},
		{
			MethodName: "getAttr",
			Handler:    _TeamIssueAttrApi_GetAttr_Handler,
		},
		{
			MethodName: "removeAttr",
			Handler:    _TeamIssueAttrApi_RemoveAttr_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "team_issue_attr_api.proto",
}
